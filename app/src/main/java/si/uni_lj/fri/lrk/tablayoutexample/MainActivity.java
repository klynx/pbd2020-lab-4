package si.uni_lj.fri.lrk.tablayoutexample;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final int NUM_OF_TABS = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        configureTabLayout();
    }

    private void configureTabLayout(){

        TabLayout tl = findViewById(R.id.tab_layout);
        ViewPager2 vp2 = findViewById(R.id.viewPager2);
        TabPagerAdapter tpa = new TabPagerAdapter(this, 3);
        vp2.setAdapter(tpa);

        new TabLayoutMediator(tl, vp2,
                (tab, position) -> {
                    switch(position) {
                        case 0:
                            tab.setText(R.string.tab1_name);
                            break;
                        case 1:
                            tab.setText(R.string.tab2_name);
                            break;
                        case 2:
                            tab.setText(R.string.tab3_name);
                            break;
                        default:
                            Log.e("TabMediator", "wrong tab number");
                    }
                }).attach();
    }

}
